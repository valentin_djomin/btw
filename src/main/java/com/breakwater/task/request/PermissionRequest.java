
package com.breakwater.task.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "departmentId",
    "userId",
    "permissionType"
})
public class PermissionRequest implements Serializable
{

    @JsonProperty("departmentId")
    private String departmentId;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("permissionType")
    private PermissionRequest.PermissionType permissionType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 6245168604875856708L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PermissionRequest() {
    }

    /**
     * 
     * @param permissionType
     * @param departmentId
     * @param userId
     */
    public PermissionRequest(String departmentId, String userId, PermissionRequest.PermissionType permissionType) {
        super();
        this.departmentId = departmentId;
        this.userId = userId;
        this.permissionType = permissionType;
    }

    @JsonProperty("departmentId")
    public String getDepartmentId() {
        return departmentId;
    }

    @JsonProperty("departmentId")
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("permissionType")
    public PermissionRequest.PermissionType getPermissionType() {
        return permissionType;
    }

    @JsonProperty("permissionType")
    public void setPermissionType(PermissionRequest.PermissionType permissionType) {
        this.permissionType = permissionType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(PermissionRequest.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("departmentId");
        sb.append('=');
        sb.append(((this.departmentId == null)?"<null>":this.departmentId));
        sb.append(',');
        sb.append("userId");
        sb.append('=');
        sb.append(((this.userId == null)?"<null>":this.userId));
        sb.append(',');
        sb.append("permissionType");
        sb.append('=');
        sb.append(((this.permissionType == null)?"<null>":this.permissionType));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.permissionType == null)? 0 :this.permissionType.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.userId == null)? 0 :this.userId.hashCode()));
        result = ((result* 31)+((this.departmentId == null)? 0 :this.departmentId.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PermissionRequest) == false) {
            return false;
        }
        PermissionRequest rhs = ((PermissionRequest) other);
        return (((((this.permissionType == rhs.permissionType)||((this.permissionType!= null)&&this.permissionType.equals(rhs.permissionType)))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.userId == rhs.userId)||((this.userId!= null)&&this.userId.equals(rhs.userId))))&&((this.departmentId == rhs.departmentId)||((this.departmentId!= null)&&this.departmentId.equals(rhs.departmentId))));
    }

    public enum PermissionType {

        EDIT("Edit"),
        VIEW("View"),
        NONE("None");
        private final String value;
        private final static Map<String, PermissionRequest.PermissionType> CONSTANTS = new HashMap<String, PermissionRequest.PermissionType>();

        static {
            for (PermissionRequest.PermissionType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private PermissionType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static PermissionRequest.PermissionType fromValue(String value) {
            PermissionRequest.PermissionType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
