package com.breakwater.task.permission.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@AllArgsConstructor
@Document
public class GrantedPermission {

  @Id
  UUID id;
  UUID departmentId;
  UUID userId;
  UUID permissionId;
  LocalDateTime created;

}
