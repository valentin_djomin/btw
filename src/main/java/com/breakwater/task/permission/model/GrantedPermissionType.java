package com.breakwater.task.permission.model;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum GrantedPermissionType {

  ASSIGNED("Assigned"),
  INHERITED("Inherited"),
  NONE("None");

  String value;
}
