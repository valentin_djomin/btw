package com.breakwater.task.permission.service;

import static com.breakwater.task.permission.model.GrantedPermissionType.ASSIGNED;
import static com.breakwater.task.permission.model.GrantedPermissionType.INHERITED;

import com.breakwater.task.department.dto.DepartmentDTO;
import com.breakwater.task.department.service.DepartmentService;
import com.breakwater.task.exception.DepartmentNotFoundException;
import com.breakwater.task.exception.GrantedPermissionNotFoundException;
import com.breakwater.task.exception.PermissionNotFoundException;
import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.GrantedPermission;
import com.breakwater.task.permission.repository.GrantedPermissionRepository;
import com.breakwater.task.permission.repository.PermissionRepository;
import com.breakwater.task.request.PermissionRequest;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class PermissionService {

  private final DepartmentService departmentService;
  private final GrantedPermissionRepository grantedPermissionRepository;
  private final PermissionRepository permissionRepository;

  public Mono<GrantedPermission> assign(final PermissionRequest permissionRequest) {
    // TODO: 16.05.20 Could be added additional validation for request dto data.
    var departmentId = UUID.fromString(permissionRequest.getDepartmentId());
    var userId = UUID.fromString(permissionRequest.getUserId());
    var permissionType = permissionRequest.getPermissionType();

    return permissionRepository.findByValue(permissionType.value())
        .switchIfEmpty(
            Mono.error(
                new PermissionNotFoundException(
                    String.format("Permission not found for user id %s and department id %s", userId, departmentId))))
        .map(permission ->
            new GrantedPermission(
                UUID.randomUUID(),
                departmentId, // TODO: 16.05.20 Could be added department existence validation.
                userId, // TODO: 16.05.20 Could be added user existence validation.
                permission.getId(),
                LocalDateTime.now())
        ).flatMap(grantedPermissionRepository::save);
  }

  public Mono<GrantedPermission> revoke(final String grantedPermissionId) {
    var grantedPermissionUUID = UUID.fromString(grantedPermissionId);
    return grantedPermissionRepository.findById(grantedPermissionUUID)
        .switchIfEmpty(
            Mono.error(
                new GrantedPermissionNotFoundException(String.format("Granted permission not found for id: %s", grantedPermissionUUID))))
        .flatMap(grantedPermission ->
            grantedPermissionRepository
                .deleteById(grantedPermission.getId())
                .thenReturn(grantedPermission));
  }


  public Mono<PermissionDTO> retrieve(final String departmentId, final String userId) {
    // TODO: 16.05.20 Could be added additional validation for ids.
    var departmentUUID = UUID.fromString(departmentId);
    var userUUID = UUID.fromString(userId);
    return departmentService.readDepartmentParentsWithPermission(departmentUUID, userUUID)
        .switchIfEmpty(
            Mono.error(new DepartmentNotFoundException(String.format("Department not found for id: %s", departmentId))))
        .flatMap(departmentWithParents ->
            Stream.of(getDepartmentWithPermission(departmentWithParents))
                .filter(departmentWithPermission -> Objects.nonNull(departmentWithPermission.getGrantedPermission()))
                .map(departmentDTO -> permissionRepository.findById(departmentDTO.getGrantedPermission().getPermissionId())
                    .map(permission -> new PermissionDTO(
                        permission.getValue(),
                        departmentUUID.equals(departmentDTO.getGrantedPermission().getDepartmentId()) ? ASSIGNED : INHERITED,
                        departmentDTO.getGrantedPermission().getCreated()
                    )))
                .findAny()
                .orElse(Mono.empty()));
  }

  private DepartmentDTO getDepartmentWithPermission(final DepartmentDTO department) {
    if (Objects.nonNull(department.getGrantedPermission()) || Objects.isNull(department.getParent())) {
      return department;
    }
    return getDepartmentWithPermission(department.getParent());
  }
}
