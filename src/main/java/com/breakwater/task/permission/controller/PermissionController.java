package com.breakwater.task.permission.controller;

import static com.breakwater.task.constant.RequestMappingConstants.API_PREFFIX;
import static com.breakwater.task.constant.RequestMappingConstants.PERMISSION_PREFFIX;

import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.GrantedPermission;
import com.breakwater.task.permission.service.PermissionService;
import com.breakwater.task.request.PermissionRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(API_PREFFIX + "/" + PERMISSION_PREFFIX)
@RequiredArgsConstructor
public class PermissionController {

  private final PermissionService permissionService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  private Mono<GrantedPermission> assign(@RequestBody PermissionRequest permissionRequest) {
    return permissionService.assign(permissionRequest);
  }

  @DeleteMapping("{grantedPermissionId}")
  private Mono<GrantedPermission> revoke(@PathVariable String grantedPermissionId) {
    return permissionService.revoke(grantedPermissionId);
  }

  @GetMapping
  private Mono<PermissionDTO> retrieve(@RequestParam String departmentId, @RequestParam String userId) {
    return permissionService.retrieve(departmentId, userId);
  }
}
