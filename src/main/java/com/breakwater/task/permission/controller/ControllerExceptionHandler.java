package com.breakwater.task.permission.controller;

import com.breakwater.task.exception.DepartmentNotFoundException;
import com.breakwater.task.exception.GrantedPermissionNotFoundException;
import com.breakwater.task.exception.PermissionNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(GrantedPermissionNotFoundException.class)
  public ResponseEntity<String> handleGrantedPermissionNotFoundException(final GrantedPermissionNotFoundException ex) {
    log.error("Exception caught in handleGrantedPermissionNotFoundException: {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }

  @ExceptionHandler(PermissionNotFoundException.class)
  public ResponseEntity<String> handlePermissionNotFoundException(final PermissionNotFoundException ex) {
    log.error("Exception caught in handlePermissionNotFoundException: {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }

  @ExceptionHandler(DepartmentNotFoundException.class)
  public ResponseEntity<String> handleDepartmentNotFoundException(final DepartmentNotFoundException ex) {
    log.error("Exception caught in handleDepartmentNotFoundException: {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }

  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity<String> handleRuntimeException(final RuntimeException ex) {
    log.error("Exception caught in handleRuntimeException: {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handleException(final Exception ex) {
    log.error("Exception caught in handleException: {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }
}
