package com.breakwater.task.permission.repository;

import com.breakwater.task.permission.model.Permission;
import java.util.UUID;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;


@Repository
public interface PermissionRepository extends ReactiveMongoRepository<Permission, UUID> {

  Mono<Permission> findByValue(String value);
}
