package com.breakwater.task.permission.repository;


import com.breakwater.task.permission.model.GrantedPermission;
import java.util.UUID;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface GrantedPermissionRepository extends ReactiveMongoRepository<GrantedPermission, UUID> {

  Mono<GrantedPermission> findByDepartmentIdAndUserId(UUID departmentId, UUID userId);

}
