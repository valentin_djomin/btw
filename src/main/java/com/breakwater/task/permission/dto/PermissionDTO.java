package com.breakwater.task.permission.dto;

import com.breakwater.task.permission.model.GrantedPermissionType;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDTO {

  String permissionValue;
  GrantedPermissionType type;
  LocalDateTime createdAt;

}
