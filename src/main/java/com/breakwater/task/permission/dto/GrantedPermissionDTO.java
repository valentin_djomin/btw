package com.breakwater.task.permission.dto;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Value;

@Value
public class GrantedPermissionDTO {

  UUID id;
  UUID departmentId;
  UUID userId;
  UUID permissionId;
  LocalDateTime created;

}
