package com.breakwater.task.constant;

public class RequestMappingConstants {

  public static final String API_PREFFIX = "api";
  public static final String PERMISSION_PREFFIX = "permission";

}
