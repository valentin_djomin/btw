package com.breakwater.task.exception;

public class DepartmentNotFoundException extends RuntimeException {

  public DepartmentNotFoundException(String message) {
    super(message);
  }
}
