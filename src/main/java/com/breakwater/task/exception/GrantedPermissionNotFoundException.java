package com.breakwater.task.exception;

public class GrantedPermissionNotFoundException extends RuntimeException {

  public GrantedPermissionNotFoundException(String message) {
    super(message);
  }
}
