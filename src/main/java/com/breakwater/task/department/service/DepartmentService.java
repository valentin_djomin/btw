package com.breakwater.task.department.service;

import com.breakwater.task.department.dto.DepartmentDTO;
import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.dto.GrantedPermissionDTO;
import com.breakwater.task.permission.repository.GrantedPermissionRepository;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class DepartmentService {

  private final DepartmentRepository departmentRepository;
  private final GrantedPermissionRepository grantedPermissionRepository;

  public Mono<DepartmentDTO> readDepartmentWithParents(UUID departmentId) {
    return departmentRepository.findById(departmentId)
        .flatMap(department -> addParent(department.getParentId(), department));
  }

  public Mono<DepartmentDTO> readDepartmentWithChildren(UUID departmentId) {
    return departmentRepository.findById(departmentId)
        .flatMap(this::addChildren);
  }

  private Mono<DepartmentDTO> addParent(UUID parentId, Department department) {
    return Mono.justOrEmpty(parentId)
        .flatMap(departmentRepository::findById)
        .flatMap(parentDepartment -> addParent(parentDepartment.getParentId(), parentDepartment))
        .map(parentDepartmentDTO -> new DepartmentDTO(department.getId(), department.getName(), parentDepartmentDTO, List.of(), null))
        .switchIfEmpty(Mono.just(new DepartmentDTO(department.getId(), department.getName(), null, List.of(), null)));
  }

  private Mono<DepartmentDTO> addChildren(Department department) {
    return departmentRepository.findByParentId(department.getId())
        .flatMap(this::addChildren)
        .collectList()
        .map(childDepartments -> new DepartmentDTO(department.getId(), department.getName(), null, childDepartments, null));
  }

  public Mono<DepartmentDTO> readDepartmentParentsWithPermission(UUID departmentId, UUID userId) {
    return departmentRepository.findById(departmentId)
        .flatMap(department -> addParentWithPermission(department.getParentId(), userId, department));
  }

  private Mono<DepartmentDTO> addParentWithPermission(UUID parentId, UUID userId, Department department) {
    return Mono.justOrEmpty(parentId)
        .flatMap(departmentRepository::findById)
        .flatMap(parentDepartment -> grantedPermissionRepository
            .findByDepartmentIdAndUserId(department.getId(), userId)
            .map(e -> new DepartmentDTO(department.getId(), department.getName(), null, List.of(), null))
            .switchIfEmpty(addParentWithPermission(parentDepartment.getParentId(), userId, parentDepartment)))
        .flatMap(parentDepartmentDTO -> grantedPermissionRepository.findByDepartmentIdAndUserId(department.getId(), userId)
            .map(grantedPermission -> {
              GrantedPermissionDTO grantedPermission1 = new GrantedPermissionDTO(
                  grantedPermission.getId(),
                  grantedPermission.getDepartmentId(),
                  grantedPermission.getUserId(),
                  grantedPermission.getPermissionId(),
                  grantedPermission.getCreated());
              return new DepartmentDTO(department.getId(), department.getName(), parentDepartmentDTO, List.of(), grantedPermission1);
            })
            .switchIfEmpty(Mono.just(new DepartmentDTO(department.getId(), department.getName(), parentDepartmentDTO, List.of(), null))))
        .switchIfEmpty(Mono.just(new DepartmentDTO(department.getId(), department.getName(), null, List.of(), null)));
  }

}
