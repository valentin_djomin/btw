package com.breakwater.task.department.dto;

import com.breakwater.task.permission.dto.GrantedPermissionDTO;
import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.Permission;
import java.util.List;
import java.util.UUID;
import lombok.Value;

@Value
public class DepartmentDTO {

  UUID id;
  String name;

  DepartmentDTO parent;
  List<DepartmentDTO> children;

  GrantedPermissionDTO grantedPermission;
}
