package com.breakwater.task.permission.service;

import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.exception.PermissionNotFoundException;
import com.breakwater.task.permission.model.GrantedPermission;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.permission.repository.GrantedPermissionRepository;
import com.breakwater.task.permission.repository.PermissionRepository;
import com.breakwater.task.request.PermissionRequest;
import com.breakwater.task.request.PermissionRequest.PermissionType;
import com.breakwater.task.user.model.User;
import com.breakwater.task.user.repository.UserRepository;
import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class PermissionServiceTest {

  @Autowired
  PermissionService permissionService;
  @Autowired
  GrantedPermissionRepository grantedPermissionRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  DepartmentRepository departmentRepository;
  @Autowired
  PermissionRepository permissionRepository;

  private static final UUID USER_ID = UUID.randomUUID();
  private static final UUID DEPARTMENT_ID = UUID.randomUUID();
  private static final UUID PERMISSION_ID = UUID.randomUUID();

  @BeforeEach
  void setup() {
    var user = new User(USER_ID, "User");
    var department = new Department(DEPARTMENT_ID, "Department", null);
    var permission = new Permission(PERMISSION_ID, "View");

    grantedPermissionRepository.deleteAll()
        .then(userRepository.deleteAll())
        .then(departmentRepository.deleteAll())
        .then(permissionRepository.deleteAll())
        .then(userRepository.save(user))
        .then(departmentRepository.save(department))
        .then(permissionRepository.save(permission))
        .block();
  }

  @Test
  void assign_success() {
    var permissionRequest = new PermissionRequest(DEPARTMENT_ID.toString(), USER_ID.toString(), PermissionType.VIEW);
    Mono<GrantedPermission> assignedGrantedPermission = permissionService.assign(permissionRequest);

    // Returned data check
    StepVerifier.create(assignedGrantedPermission.log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            USER_ID.equals(grantedPermission.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission.getPermissionId()))
        .verifyComplete();
    // Database state check
    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            USER_ID.equals(grantedPermission.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission.getPermissionId()))
        .verifyComplete();
  }

  @Test
  void assign_permissionNotFound() {
    var permissionRequest = new PermissionRequest(DEPARTMENT_ID.toString(), USER_ID.toString(), PermissionType.NONE);

    Mono<GrantedPermission> assignedGrantedPermission = permissionService.assign(permissionRequest);
    // Returned data check
    StepVerifier.create(assignedGrantedPermission.log())
        .expectSubscription()
        .expectError(PermissionNotFoundException.class);
    // Database state check
    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();
  }

  @Test
  void revoke_success() {
    var grantedPermissionId = UUID.randomUUID();
    var grantedPermission = new GrantedPermission(grantedPermissionId, DEPARTMENT_ID, USER_ID, PERMISSION_ID, LocalDateTime.now());
    grantedPermissionRepository.save(grantedPermission).subscribe();

    Mono<GrantedPermission> revokedGrantedPermission = permissionService.revoke(grantedPermissionId.toString());
    // Returned data check
    StepVerifier.create(revokedGrantedPermission.log())
        .expectSubscription()
        .expectNextMatches(grantedPermission1 ->
            grantedPermissionId.equals(grantedPermission1.getId()) &&
                USER_ID.equals(grantedPermission1.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission1.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission1.getPermissionId()))
        .verifyComplete();
    // Database state check
    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();
  }

  @Test
  void revoke_permissionNotFound() {
    var grantedPermissionId = UUID.randomUUID();
    var grantedPermission = new GrantedPermission(grantedPermissionId, DEPARTMENT_ID, USER_ID, PERMISSION_ID, LocalDateTime.now());
    grantedPermissionRepository.save(grantedPermission).subscribe();

    Mono<GrantedPermission> revokedGrantedPermission = permissionService.revoke(grantedPermissionId.toString());
    // Returned data check
    StepVerifier.create(revokedGrantedPermission.log())
        .expectSubscription()
        .expectNextMatches(grantedPermission1 ->
            grantedPermissionId.equals(grantedPermission1.getId()) &&
                USER_ID.equals(grantedPermission1.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission1.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission1.getPermissionId()))
        .verifyComplete();
    // Database state check
    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();
  }



}
