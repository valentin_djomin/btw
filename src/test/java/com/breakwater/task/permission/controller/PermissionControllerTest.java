package com.breakwater.task.permission.controller;

import static com.breakwater.task.constant.RequestMappingConstants.API_PREFFIX;
import static com.breakwater.task.constant.RequestMappingConstants.PERMISSION_PREFFIX;
import static com.breakwater.task.permission.model.GrantedPermissionType.INHERITED;

import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.model.GrantedPermission;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.permission.repository.GrantedPermissionRepository;
import com.breakwater.task.permission.repository.PermissionRepository;
import com.breakwater.task.request.PermissionRequest;
import com.breakwater.task.request.PermissionRequest.PermissionType;
import com.breakwater.task.user.model.User;
import com.breakwater.task.user.repository.UserRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
class PermissionControllerTest {

  @Autowired
  WebTestClient webTestClient;
  @Autowired
  GrantedPermissionRepository grantedPermissionRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  DepartmentRepository departmentRepository;
  @Autowired
  PermissionRepository permissionRepository;

  private static final UUID USER_ID = UUID.randomUUID();
  private static final UUID DEPARTMENT_ID = UUID.randomUUID();
  private static final UUID PERMISSION_ID = UUID.randomUUID();

  @BeforeEach
  void setup() {
    var user = new User(USER_ID, "User");
    var department = new Department(DEPARTMENT_ID, "Department", null);
    var permission = new Permission(PERMISSION_ID, "View");

    grantedPermissionRepository.deleteAll()
        .then(userRepository.deleteAll())
        .then(departmentRepository.deleteAll())
        .then(permissionRepository.deleteAll())
        .then(userRepository.save(user))
        .then(departmentRepository.save(department))
        .then(permissionRepository.save(permission))
        .block();
  }

  @Test
  void assign_success() {
    var permissionRequest = new PermissionRequest(DEPARTMENT_ID.toString(), USER_ID.toString(), PermissionType.VIEW);

    webTestClient.post().uri("/" + API_PREFFIX + "/" + PERMISSION_PREFFIX)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .body(Mono.just(permissionRequest), PermissionRequest.class)
        .exchange()
        .expectStatus().isCreated()
        .expectBody()
        .jsonPath("$.id").isNotEmpty()
        .jsonPath("$.departmentId").isEqualTo(DEPARTMENT_ID.toString())
        .jsonPath("$.userId").isEqualTo(USER_ID.toString())
        .jsonPath("$.permissionId").isNotEmpty()
        .jsonPath("$.created").isNotEmpty();

    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            USER_ID.equals(grantedPermission.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission.getPermissionId()))
        .verifyComplete();
  }

  @Test
  void assign_permissionNotFound() {
    var permissionRequest = new PermissionRequest(DEPARTMENT_ID.toString(), USER_ID.toString(), PermissionType.NONE);

    webTestClient.post().uri("/" + API_PREFFIX + "/" + PERMISSION_PREFFIX)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .body(Mono.just(permissionRequest), PermissionRequest.class)
        .exchange()
        .expectStatus().is5xxServerError()
        .expectBody(String.class)
        .isEqualTo(String.format("Permission not found for user id %s and department id %s", USER_ID.toString(), DEPARTMENT_ID.toString()));

    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();
  }

  @Test
  void revoke_success() {
    var grantedPermissionId = UUID.randomUUID();
    var grantedPermission = new GrantedPermission(grantedPermissionId, DEPARTMENT_ID, USER_ID, PERMISSION_ID, LocalDateTime.now());
    grantedPermissionRepository.save(grantedPermission).subscribe();

    var revokeUri = "/" + API_PREFFIX + "/" + PERMISSION_PREFFIX + "/{grantedPermissionId}";
    webTestClient.delete().uri(revokeUri, grantedPermissionId)
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$.id").isNotEmpty()
        .jsonPath("$.departmentId").isEqualTo(DEPARTMENT_ID.toString())
        .jsonPath("$.userId").isEqualTo(USER_ID.toString())
        .jsonPath("$.permissionId").isEqualTo(PERMISSION_ID.toString())
        .jsonPath("$.created").isNotEmpty();

    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();
  }

  @Test
  void revoke_grantedPermissionNotFound() {
    var grantedPermissionId = UUID.randomUUID();
    var revokeUri = "/" + API_PREFFIX + "/" + PERMISSION_PREFFIX + "/{grantedPermissionId}";
    webTestClient.delete().uri(revokeUri, grantedPermissionId)
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .exchange()
        .expectStatus().is5xxServerError()
        .expectBody(String.class)
        .isEqualTo(String.format("Granted permission not found for id: %s", grantedPermissionId));
  }

  @Test
  void retrieve_departmentNotFound() {
    var departmentId = UUID.randomUUID();
    var retrieveUri = "/" + API_PREFFIX + "/" + PERMISSION_PREFFIX;
    webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path(retrieveUri)
                .queryParam("departmentId", departmentId)
                .queryParam("userId", UUID.randomUUID())
                .build())
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .exchange()
        .expectStatus().is5xxServerError()
        .expectBody(String.class)
        .isEqualTo(String.format("Department not found for id: %s", departmentId));
  }

  @Test
  void retrieve_noPermissionGranted() {
    var companyId = UUID.fromString("5a0bbdbe-c872-4467-9070-b5284d7b658f");
    var financeId = UUID.fromString("57633250-8e65-4b2a-b814-5838a1b8d3ff");
    var accountsReceivableId = UUID.fromString("751edf8c-e1da-4d2a-9f1a-d1a5509ebdbb");
    var company = new Department(companyId, "Company", null);
    var finance = new Department(financeId, "Finance", companyId);
    var accountsReceivable = new Department(accountsReceivableId, "Accounts Receivable", financeId);
    var departments = List.of(company, finance, accountsReceivable);

    departmentRepository.insert(departments).subscribe();

    var retrieveUri = "/" + API_PREFFIX + "/" + PERMISSION_PREFFIX;
    webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path(retrieveUri)
                .queryParam("departmentId", accountsReceivableId)
                .queryParam("userId", UUID.randomUUID())
                .build())
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .exchange()
        .expectStatus().isOk()
        .expectBody(String.class)
        .isEqualTo(null);
  }

  @Test
  void retrieve_success() {
    var companyId = UUID.fromString("5a0bbdbe-c872-4467-9070-b5284d7b658f");
    var financeId = UUID.fromString("57633250-8e65-4b2a-b814-5838a1b8d3ff");
    var accountsReceivableId = UUID.fromString("751edf8c-e1da-4d2a-9f1a-d1a5509ebdbb");
    var company = new Department(companyId, "Company", null);
    var finance = new Department(financeId, "Finance", companyId);
    var accountsReceivable = new Department(accountsReceivableId, "Accounts Receivable", financeId);
    var departments = List.of(company, finance, accountsReceivable);

    var joeId = UUID.fromString("47c1a9b6-ea34-44f0-a157-411880c67003");
    var joe = new User(joeId, "Joe");

    var editId = UUID.fromString("b8cd1f6f-1666-4eb6-a2c8-3a372770731f");
    var edit = new Permission(editId, "Edit");

    var financeJoeEditId = UUID.fromString("40034d19-c557-4cba-22d4-9713d614f284");
    var financeJoe = new GrantedPermission(financeJoeEditId, financeId, joeId, editId, LocalDateTime.now());


    departmentRepository.insert(departments)
        .then(userRepository.insert(joe))
        .then(permissionRepository.insert(edit))
        .then(grantedPermissionRepository.insert(financeJoe))
        .block();

    var retrieveUri = "/" + API_PREFFIX + "/" + PERMISSION_PREFFIX;
    webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path(retrieveUri)
                .queryParam("departmentId", accountsReceivableId)
                .queryParam("userId", joeId)
                .build())
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$.permissionValue").isEqualTo("Edit")
        .jsonPath("$.type").isEqualTo(INHERITED.name());
  }

}
