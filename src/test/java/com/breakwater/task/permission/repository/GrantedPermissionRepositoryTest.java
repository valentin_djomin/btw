package com.breakwater.task.permission.repository;

import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.model.GrantedPermission;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.user.model.User;
import com.breakwater.task.user.repository.UserRepository;
import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@DataMongoTest
@ExtendWith(SpringExtension.class)
class GrantedPermissionRepositoryTest {

  @Autowired
  GrantedPermissionRepository grantedPermissionRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  DepartmentRepository departmentRepository;
  @Autowired
  PermissionRepository permissionRepository;

  private static final UUID USER_ID = UUID.randomUUID();
  private static final UUID DEPARTMENT_ID = UUID.randomUUID();
  private static final UUID PERMISSION_ID = UUID.randomUUID();
  private static final UUID GRANTED_PERMISSION_ID = UUID.randomUUID();

  @BeforeEach
  void setup() {
    var user = new User(USER_ID, "User");
    var department = new Department(DEPARTMENT_ID, "Department", null);
    var permission = new Permission(PERMISSION_ID, "Permission");
    var grantedPermission = new GrantedPermission(GRANTED_PERMISSION_ID, DEPARTMENT_ID, USER_ID, PERMISSION_ID, LocalDateTime.now());

    grantedPermissionRepository.deleteAll()
        .then(userRepository.deleteAll())
        .then(departmentRepository.deleteAll())
        .then(permissionRepository.deleteAll())
        .then(userRepository.save(user))
        .then(departmentRepository.save(department))
        .then(permissionRepository.save(permission))
        .then(grantedPermissionRepository.save(grantedPermission))
        .block();
  }


  @Test
  void findAll() {
    StepVerifier.create(grantedPermissionRepository.findAll().log())
        .expectSubscription()
        .expectNextCount(1)
        .verifyComplete();
  }

  @Test
  void findById() {
    StepVerifier.create(grantedPermissionRepository.findById(GRANTED_PERMISSION_ID).log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            USER_ID.equals(grantedPermission.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission.getPermissionId()))
        .verifyComplete();
  }

  @Test
  void findByDepartmentIdAndUserId() {
    StepVerifier.create(grantedPermissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID).log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            GRANTED_PERMISSION_ID.equals(grantedPermission.getId())
                && DEPARTMENT_ID.equals(grantedPermission.getDepartmentId())
                && USER_ID.equals(grantedPermission.getUserId()))
        .verifyComplete();
  }

  @Test
  void save() {
    var userId2 = UUID.randomUUID();
    var user2 = new User(userId2, "User2");

    var grantedPermissionId2 = UUID.randomUUID();
    var grantedPermission2 = new GrantedPermission(grantedPermissionId2, DEPARTMENT_ID, userId2, PERMISSION_ID, LocalDateTime.now());

    Mono<GrantedPermission> savedGrantedPermission = userRepository.save(user2)
        .then(grantedPermissionRepository.save(grantedPermission2));

    StepVerifier.create(savedGrantedPermission.log())
        .expectSubscription()
        .expectNextMatches(grantedPermission1 ->
            userId2.equals(grantedPermission1.getUserId()) &&
                DEPARTMENT_ID.equals(grantedPermission1.getDepartmentId()) &&
                PERMISSION_ID.equals(grantedPermission1.getPermissionId()))
        .verifyComplete();
  }

  @Test
  void update() {
    var userId2 = UUID.randomUUID();
    var user2 = new User(userId2, "User2");

    userRepository.save(user2);

    Mono<GrantedPermission> updatedGrantedPermission = grantedPermissionRepository.findById(GRANTED_PERMISSION_ID)
        .map(grantedPermission -> {
          grantedPermission.setUserId(user2.getId());
          return grantedPermission;
        })
        .flatMap(grantedPermissionRepository::save);

    StepVerifier.create(updatedGrantedPermission.log())
        .expectSubscription()
        .expectNextMatches(grantedPermission ->
            GRANTED_PERMISSION_ID.equals(grantedPermission.getId()) &&
                userId2.equals(grantedPermission.getUserId()))
        .verifyComplete();
  }

  @Test
  void delete() {
    final Mono<Void> deletedPermission = grantedPermissionRepository.findById(GRANTED_PERMISSION_ID)
        .map(GrantedPermission::getId)
        .flatMap(uuid -> grantedPermissionRepository.deleteById(uuid));

    StepVerifier.create(deletedPermission.log())
        .expectSubscription()
        .verifyComplete();

    StepVerifier.create(grantedPermissionRepository.findAll().log())
        .expectSubscription()
        .expectNextCount(0)
        .verifyComplete();

  }


}
