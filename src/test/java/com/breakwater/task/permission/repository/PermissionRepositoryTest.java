package com.breakwater.task.permission.repository;

import com.breakwater.task.permission.model.Permission;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@DataMongoTest
@ExtendWith(SpringExtension.class)
class PermissionRepositoryTest {

  @Autowired
  PermissionRepository permissionRepository;

  public static final UUID CREATE_ID = UUID.randomUUID();
  public static final UUID READ_ID = UUID.randomUUID();
  public static final UUID UPDATE_ID = UUID.randomUUID();
  public static final UUID DELETE_ID = UUID.randomUUID();

  final List<Permission> permissions = Arrays.asList(
      new Permission(CREATE_ID, "Create"),
      new Permission(READ_ID, "Read"),
      new Permission(UPDATE_ID, "Update"),
      new Permission(DELETE_ID, "Delete")
  );

  @BeforeEach
  void setup() {
    permissionRepository.deleteAll()
        .thenMany(Flux.fromIterable(permissions))
        .flatMap(permissionRepository::save)
        .doOnNext(permission -> {
          System.out.println("Inserted permission is: " + permission);
        })
        .blockLast();
  }


  @Test
  void findAll() {
    StepVerifier.create(permissionRepository.findAll().log())
        .expectSubscription()
        .expectNextCount(4)
        .verifyComplete();
  }

  @Test
  void findById() {
    StepVerifier.create(permissionRepository.findById(CREATE_ID).log())
        .expectSubscription()
        .expectNextMatches(permission -> "Create".equals(permission.getValue()))
        .verifyComplete();
  }

  @Test
  void findByValue() {
    StepVerifier.create(permissionRepository.findByValue("Delete").log())
        .expectSubscription()
        .expectNextMatches(permission ->
            DELETE_ID.equals(permission.getId()) && "Delete".equals(permission.getValue()))
        .verifyComplete();
  }

  @Test
  void save() {
    final UUID permissionUUID = UUID.randomUUID();
    final String permissionValue = "Test";
    final Permission permission = new Permission(permissionUUID, permissionValue);
    final Mono<Permission> savedPermission = permissionRepository.save(permission);

    StepVerifier.create(savedPermission.log())
        .expectSubscription()
        .expectNextMatches(permission1 ->
            permissionUUID.equals(permission1.getId()) && permissionValue.equals(permission1.getValue()))
        .verifyComplete();
  }

  @Test
  void update() {
    final String permissionUpdatedValue = "UpdatedValue";

    final Mono<Permission> updatedPermission = permissionRepository.findById(READ_ID)
        .map(permission -> {
          permission.setValue(permissionUpdatedValue);
          return permission;
        })
        .flatMap(permissionRepository::save);

    StepVerifier.create(updatedPermission.log())
        .expectSubscription()
        .expectNextMatches(permission ->
            READ_ID.equals(permission.getId()) && permissionUpdatedValue.equals(permission.getValue()))
        .verifyComplete();
  }

  @Test
  void delete() {
    final Mono<Void> deletedPermission = permissionRepository.findById(READ_ID)
        .map(Permission::getId)
        .flatMap(uuid -> permissionRepository.deleteById(uuid));

    StepVerifier.create(deletedPermission.log())
        .expectSubscription()
        .verifyComplete();

    StepVerifier.create(permissionRepository.findAll().log())
        .expectSubscription()
        .expectNextCount(3)
        .verifyComplete();

  }
}
